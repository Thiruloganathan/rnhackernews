//fix comment text special character issues
export const fixComment = str => {
  return String(str)
    .replace(/<p>/g, '\n\n')
    .replace(/&#x2F;/g, '/')
    .replace('<i>', '')
    .replace('</i>', '')
    .replace(/&#x27;/g, "'")
    .replace(/&quot;/g, '"')
    .replace(
      /<a\s+(?:[^>]*?\s+)?href="([^"]*)" rel="nofollow">(.*)?<\/a>/g,
      '$1',
    );
};

//capitalize first letter from string
export const Capitalize = str => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};
