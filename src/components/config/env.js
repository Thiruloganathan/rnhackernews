//import liraries
import {API_BASE_URL} from '@env';

const devEnvironmentVariables = {
  API_BASE_URL,
};

const prodEnvironmentVariables = {
  API_BASE_URL,
};

export default __DEV__ ? devEnvironmentVariables : prodEnvironmentVariables;
