//api callbacks
import * as Network from './network';
import * as ApiConstants from './constants';

//get top story ids
export const getTopStoryIDs = () => {
  return Network.topStoriesGetApiCall(ApiConstants.TopStoriesIds);
};

//get top stories based on list of ids
export const getTopStoriesById = id => {
  return Network.topStoryByIdApiCall(id, ApiConstants.ItemById);
};

//get comments by kids ids
export const getCommentsByKidsId = id => {
  return Network.commentsByKidsIdApiCall(id, ApiConstants.ItemById);
};
