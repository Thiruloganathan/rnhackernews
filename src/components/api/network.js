//api callbacks
import axios from 'axios';
import env from '../config/env';

//load top stories api callback to get list of stories
export const topStoriesGetApiCall = apiEndPoint => {
  return axios({
    method: 'GET',
    url: env.API_BASE_URL + apiEndPoint,
  })
    .then(response => {
      //console.log('res', response);
      return response;
    })
    .catch(error => {
      if (error.response?.data) {
        //console.log('error api get111 ', error.response.data);
        return error.response.data;
      }
      //console.log('error api get ', error);
      return error;
    });
};

//load top stories with id
export const topStoryByIdApiCall = (id, apiEndPoint) => {
  return axios({
    method: 'GET',
    url: `${env.API_BASE_URL}${apiEndPoint}${id}.json`,
  })
    .then(response => {
      //console.log('res', response);
      return response;
    })
    .catch(error => {
      if (error.response?.data) {
        //console.log('error api get111 ', error.response.data);
        return error.response.data;
      }
      //console.log('error api get ', error);
      return error;
    });
};

//load comments by list of kids id
export const commentsByKidsIdApiCall = (id, apiEndPoint) => {
  return axios({
    method: 'GET',
    url: `${env.API_BASE_URL}${apiEndPoint}${id}.json`,
  })
    .then(response => {
      //console.log('res', response);
      return response;
    })
    .catch(error => {
      if (error.response?.data) {
        //console.log('error api get111 ', error.response.data);
        return error.response.data;
      }
      //console.log('error api get ', error);
      return error;
    });
};
