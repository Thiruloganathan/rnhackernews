//import liraries
import React, {memo} from 'react';
import {TouchableOpacity} from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconIonicons from 'react-native-vector-icons/Ionicons';

const Icon = ({
  iconName,
  iconSize,
  iconColor,
  iconCategory,
  iconStyle,
  onIconClick,
}) => {
  return (
    <TouchableOpacity
      key={iconName}
      onPress={() => (onIconClick ? onIconClick() : null)}>
      {iconCategory == 'FontAwesome' && (
        <IconFontAwesome
          name={iconName}
          size={iconSize}
          color={iconColor}
          style={iconStyle}
        />
      )}
      {iconCategory == 'Entypo' && (
        <IconEntypo
          name={iconName}
          size={iconSize}
          color={iconColor}
          style={iconStyle}
        />
      )}
      {iconCategory == 'Ionicons' && (
        <IconIonicons
          name={iconName}
          size={iconSize}
          color={iconColor}
          style={iconStyle}
        />
      )}
    </TouchableOpacity>
  );
};

export default memo(Icon);
