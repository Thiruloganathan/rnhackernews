//import liraries
import React, {memo} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Colors} from '../../assets/colors';
// create a component
const Story = ({item, index, onClick}) => {
  return (
    <TouchableOpacity
      onPress={() => (onClick ? onClick() : null)}
      disabled={onClick ? false : true}
      style={styles.container}>
      <View style={styles.sub_container}>
        {onClick && (
          <View style={styles.story_pos_view}>
            <Text style={styles.story_pos_txt} adjustsFontSizeToFit>
              {index}
            </Text>
          </View>
        )}
        <View style={styles.flex_1}>
          <Text style={styles.story_title(onClick)} numberOfLines={3}>
            {item?.title}
          </Text>
          <View style={styles.story_info_view}>
            <Text style={styles.story_info_txt}>{`Posted by ${item?.by} | ${
              item?.score
            } Points | ${
              item?.kids?.length ? item?.kids?.length : 0
            } Comments`}</Text>
          </View>
          <View style={styles.story_item_underline} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {paddingStart: 5, paddingTop: 10, paddingEnd: 10},
  sub_container: {flexDirection: 'row', marginHorizontal: 5},
  story_pos_view: {justifyContent: 'center', marginEnd: 5, marginBottom: 10},
  story_pos_txt: {
    textAlign: 'center',
    fontSize: 20,
    width: 50,
    color: Colors.app_gray_clr,
  },
  flex_1: {flex: 1},
  story_title: onClick => ({
    fontSize: onClick ? 18 : 22,
    color: Colors.app_content_clr,
  }),
  story_info_view: {flexDirection: 'row', marginTop: 10},
  story_info_txt: {color: Colors.app_gray_clr},
  story_item_underline: {
    height: 0.5,
    backgroundColor: Colors.app_gray_clr,
    marginTop: 10,
  },
});

//make this component available to the app
export default memo(Story);
