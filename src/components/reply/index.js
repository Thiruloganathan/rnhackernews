//import liraries
import React, {memo} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Colors} from '../../assets/colors';
import * as Utils from '../../utils';
// create a component
const Reply = ({item, index}) => {
  return (
    <View key={item?.id} style={styles.container(item)}>
      {item?.text && (
        <View style={styles.flex_direction}>
          <View style={styles.cmt_name_batch_view}>
            <View style={styles.name_batch_circle_view}>
              <Text style={styles.name_batch_first_txt}>
                {Utils.Capitalize(item?.by?.charAt(0))}
              </Text>
            </View>
          </View>
          <View key={index} style={styles.flex_1}>
            <Text style={styles.name_caps_txt}>
              {Utils.Capitalize(item?.by)}
            </Text>
            <Text style={styles.cmt_description_txt}>
              {Utils.fixComment(item?.text)}
            </Text>
          </View>
        </View>
      )}
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: item => ({
    marginTop: item?.text ? 10 : 0,
    marginHorizontal: 10,
    marginBottom: item?.text ? 10 : 0,
  }),
  flex_direction: {flexDirection: 'row'},
  cmt_name_batch_view: {
    alignItems: 'center',
  },
  name_batch_circle_view: {
    borderRadius: 50,
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.app_white_clr,
    marginEnd: 10,
  },
  name_batch_first_txt: {
    color: Colors.app_content_clr,
    fontSize: 20,
    fontWeight: '700',
  },
  flex_1: {flex: 1},
  name_caps_txt: {
    color: Colors.app_content_clr,
    fontSize: 18,
    marginBottom: 10,
  },
  cmt_description_txt: {fontSize: 16},
});

//make this component available to the app
export default memo(Reply);
