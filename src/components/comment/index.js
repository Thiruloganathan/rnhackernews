//import liraries
import React, {memo} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Colors} from '../../assets/colors';
import * as Utils from '../../utils';
// create a component
const Comment = ({item, index}) => {
  let comment = item?.commentsObj;
  return (
    <View key={comment?.id} style={styles.container(comment)}>
      {comment?.text && (
        <View key={index} style={styles.flex_1}>
          <Text style={styles.cmt_user_name}>
            {Utils.Capitalize(comment?.by)}
          </Text>
          <Text style={styles.cmt_msg_description}>
            {Utils.fixComment(comment?.text)}
          </Text>
        </View>
      )}
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: comment => ({
    marginTop: comment?.text ? 10 : 0,
    marginHorizontal: 10,
  }),
  flex_1: {flex: 1},
  cmt_user_name: {
    color: Colors.app_content_clr,
    fontSize: 18,
    marginBottom: 10,
  },
  cmt_msg_description: {fontSize: 16},
});

//make this component available to the app
export default memo(Comment);
