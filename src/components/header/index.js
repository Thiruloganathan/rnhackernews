//import liraries
import React, {memo} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Colors} from '../../assets/colors';
import RNIcon from '../icons';
// create a component
const Header = props => {
  return (
    <View style={styles.container}>
      {props?.isBack && props?.isBack != null && (
        <RNIcon
          iconName={'arrow-back'}
          iconCategory={'Ionicons'}
          iconSize={25}
          iconColor={Colors.app_white_clr}
          onIconClick={() => props?.navigation?.pop()}
        />
      )}
      <Text style={styles.title_txt}>{props.title}</Text>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.app_content_clr,
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  title_txt: {
    fontSize: 20,
    color: Colors.app_white_clr,
    flex: 1,
    textAlign: 'center',
  },
});

//make this component available to the app
export default memo(Header);
