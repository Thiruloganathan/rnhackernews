//import liraries
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  RefreshControl,
} from 'react-native';
import Header from '../../components/header';
import * as ApiCall from '../../components/api';
import {Colors} from '../../assets/colors';
import StoryItem from '../../components/story';

// create a component
const Home = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [topStories, setTopStories] = useState([]);
  const [endIndex, setEndIndex] = useState(0);
  const [totalCount, setTotalCount] = useState(0);
  const [topStoryIds, setStoryIds] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  //called when user pull to refresh
  useEffect(() => {
    if (topStoryIds?.length == 0 && topStories?.length == 0) {
      getData();
    }
  }, [refreshing === true]);

  //load list for top stories
  const getData = async () => {
    await ApiCall.getTopStoryIDs().then(res => {
      //setApiRes(JSON.stringify(res));
      let topIds = res?.data;
      //console.log('Get all app--------', topIds);
      getTopStoriesByIds(topIds, 0, 20);
    });
  };

  //get Top Stories by ids
  const getTopStoriesByIds = async (ids, startIndex, limit) => {
    let rowData = [];
    let endIndex =
      startIndex + limit < ids.length ? startIndex + limit : ids.length;

    Promise.all(
      ids.slice(startIndex, endIndex).map(async function (item, i) {
        if (startIndex < endIndex) {
          await ApiCall.getTopStoriesById(item).then(res => {
            res.count = startIndex + 1;
            rowData.push(res?.data);
            startIndex++;
          });
          //console.log('Get all id data s--------', story);
        }
      }),
    ).then(() => {
      //console.log('Get all id data s--------', rowData);
      //once api response is success - then set values to state fields
      setLoading(false);
      setTopStories([...topStories, ...rowData]);
      setTotalCount(ids.length);
      setEndIndex(endIndex);
      setStoryIds(ids);
      setIsLoadMore(false);
      setRefreshing(false);
    });
  };

  return (
    <View style={styles.container}>
      <Header title={'Top Stories'} />
      {isLoading ? (
        <View style={styles.loader_view}>
          <ActivityIndicator color={Colors.app_content_clr} size={'large'} />
        </View>
      ) : (
        <FlatList
          data={topStories}
          renderItem={({item, index}) => (
            <StoryItem
              item={item}
              index={index + 1}
              onClick={() =>
                navigation.navigate('DetailScreen', {
                  data: item,
                  pos: index + 1,
                })
              }
            />
          )}
          keyExtractor={item => item.id}
          contentContainerStyle={styles.flat_list_style}
          nEndReachedThreshold={0.1}
          onEndReached={async () => {
            //to call load more method to get additional data based on page number
            if (totalCount > topStories?.length && !isLoadMore && !refreshing) {
              await setIsLoadMore(true);
              //call next set of data based on end index
              getTopStoriesByIds(topStoryIds, endIndex, 20);
              //console.log('api-response-home', totalCount, topStories);
            }
          }}
          ListFooterComponent={() => {
            //load footer view - like load more items loader & api response is empty handler
            return (
              <View>
                {topStories?.length > 0 && totalCount > topStories?.length ? (
                  <View style={styles.footer_loader_view}>
                    <ActivityIndicator
                      color={Colors.app_content_clr}
                      size={'large'}
                    />
                  </View>
                ) : topStories?.length == 0 && !isLoading && !refreshing ? (
                  <View>
                    <Text>No Data Found</Text>
                  </View>
                ) : null}
              </View>
            );
          }}
          refreshing={refreshing}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => {
                setRefreshing(true);
                setTopStories([]);
                setTotalCount(0);
                setEndIndex(0);
                setStoryIds([]);
              }}
              title="Pull to refresh"
              tintColor={Colors.app_content_clr}
              titleColor={Colors.app_content_clr}
            />
          }
        />
      )}
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loader_view: {justifyContent: 'center', alignSelf: 'center', flex: 1},
  flat_list_style: {paddingBottom: 50},
  footer_loader_view: {marginVertical: 20, marginHorizontal: 20},
});

//make this component available to the app
export default Home;
