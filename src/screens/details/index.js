//import liraries
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  RefreshControl,
} from 'react-native';
import Header from '../../components/header';
import * as ApiCall from '../../components/api';
import StoryItem from '../../components/story';
import {Colors} from '../../assets/colors';
import CommentItem from '../../components/comment';
import ReplyItem from '../../components/reply';
import * as Utils from '../../utils';

// create a component
const Details = ({route, navigation}) => {
  const {data, pos} = route?.params ? route?.params : '';

  const [comments, setComments] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [endIndex, setEndIndex] = useState(0);
  const [totalCount, setTotalCount] = useState(0);
  const [refreshing, setRefreshing] = useState(false);

  //called when user pull to refresh the data
  useEffect(() => {
    if (comments?.length == 0) {
      getCommentsData(data?.kids, 0, 20);
    }
  }, [refreshing == true]);

  //Load multiple comments based on story id with respective replies
  const getCommentsData = async (storyKids, startIndex, limit) => {
    if (storyKids?.length > 0) {
      let endIndexVal =
        startIndex + limit < storyKids?.length
          ? startIndex + limit
          : storyKids?.length;

      //console.log('res-commends', storyKids);

      let listOfComments = [];
      let commentObj = {};

      Promise.all(
        storyKids?.slice(startIndex, endIndexVal).map(async function (item, i) {
          if (startIndex < endIndexVal) {
            await ApiCall.getCommentsByKidsId(item).then(async res => {
              let replyMesKids = res?.data?.kids ? res?.data?.kids : [];
              let comments = res?.data ? res?.data : null;

              //get comment replies
              await ApiCall.getCommentsByKidsId(replyMesKids[0]).then(
                replyData => {
                  commentObj = {
                    commentsObj: comments,
                    reply: replyData?.data,
                  };
                  listOfComments.push(commentObj);
                },
              );
              //console.log('res-commends-rrr', commentObj, listOfComments);
              startIndex++;
            });
          }
        }),
      ).then(() => {
        //console.log('Get all id data s--------', listOfComments);
        //once receive all info - set the values with respective state fields
        setLoading(false);
        let oldComments = refreshing ? [] : comments;
        setComments([...oldComments, ...listOfComments]);
        setTotalCount(storyKids?.length);
        setEndIndex(endIndexVal);
        setIsLoadMore(false);
        setRefreshing(false);
      });
    } else {
      setLoading(false);
      setComments([]);
      setTotalCount(0);
      setEndIndex(0);
      setIsLoadMore(false);
      setRefreshing(false);
    }
  };

  return (
    <View style={styles.container}>
      <Header
        title={`Top Story #${pos}`}
        isBack={true}
        navigation={navigation}
      />
      <StoryItem item={data} index={pos - 1} />
      {isLoading ? (
        <View style={styles.loader_view}>
          <ActivityIndicator color={Colors.app_content_clr} size={'large'} />
        </View>
      ) : (
        <View style={styles.list_cmt_view}>
          <Text style={styles.title_cmt_txt}>{`Comments`}</Text>
          <FlatList
            data={comments}
            renderItem={({item, index}) => {
              let comment = item?.commentsObj;
              let reply = item?.reply;
              return (
                <View key={index}>
                  <View style={styles.flex_direction}>
                    {((comment && comment?.text) || (reply && reply?.text)) && (
                      <View style={styles.cmt_name_batch_view}>
                        <View style={styles.cmt_name_batch_letter_view}>
                          <Text style={styles.cmt_name_batch_letter_txt}>
                            {comment?.by
                              ? Utils.Capitalize(comment?.by?.charAt(0))
                              : ''}
                          </Text>
                        </View>
                        <View style={styles.vertical_line_view} />
                      </View>
                    )}
                    <View style={styles.flex_1}>
                      {comment && comment?.text && (
                        <CommentItem item={item} index={index} />
                      )}
                      {reply && reply?.text && (
                        <ReplyItem item={reply} index={index} />
                      )}
                      {((comment && comment?.text) ||
                        (reply && reply?.text)) && (
                        <View style={styles.horizontal_line_view} />
                      )}
                    </View>
                  </View>
                </View>
              );
            }}
            keyExtractor={item => item?.commentsObj?.id}
            contentContainerStyle={styles.flat_list_style}
            nEndReachedThreshold={0.1}
            onEndReached={() => {
              //to call load more method to get additional data based on page number
              //console.log('res-loadmore', totalCount, comments?.length);
              if (totalCount > comments?.length && !isLoadMore && !refreshing) {
                setIsLoadMore(true);
                //call next set of data based on end index
                getCommentsData(data?.kids, endIndex, 20);
                //console.log('api-response-home', totalCount, topStories);
              }
            }}
            ListFooterComponent={() => {
              //load footer view - like load more items loader & api response is empty handler
              return (
                <View>
                  {comments?.length > 0 && totalCount > comments?.length ? (
                    <View style={styles.footer_loader_view}>
                      <ActivityIndicator
                        color={Colors.app_content_clr}
                        size={'large'}
                      />
                    </View>
                  ) : comments?.length == 0 && !isLoading && !refreshing ? (
                    <View>
                      <Text>No Data Found</Text>
                    </View>
                  ) : null}
                </View>
              );
            }}
            refreshing={refreshing}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={() => {
                  setRefreshing(true);
                  setTotalCount(0);
                  setEndIndex(0);
                  setComments([]);
                }}
                title="Pull to refresh"
                tintColor={Colors.app_content_clr}
                titleColor={Colors.app_content_clr}
              />
            }
          />
        </View>
      )}
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loader_view: {justifyContent: 'center', alignSelf: 'center', flex: 1},
  list_cmt_view: {marginHorizontal: 10, paddingVertical: 10, flex: 1},
  title_cmt_txt: {
    marginBottom: 15,
    fontSize: 20,
    fontWeight: 'bold',
  },
  flex_direction: {flexDirection: 'row'},
  cmt_name_batch_view: {
    alignItems: 'center',
  },
  cmt_name_batch_letter_view: {
    borderRadius: 50,
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.app_white_clr,
    marginEnd: 10,
  },
  cmt_name_batch_letter_txt: {
    color: Colors.app_content_clr,
    fontSize: 20,
    fontWeight: '700',
  },
  vertical_line_view: {
    width: 0.5,
    flex: 1,
    backgroundColor: Colors.app_gray_clr,
    marginVertical: 15,
  },
  horizontal_line_view: {
    height: 0.5,
    backgroundColor: Colors.app_gray_clr,
    marginTop: 10,
    marginHorizontal: 10,
  },
  flex_1: {flex: 1},
  flat_list_style: {paddingBottom: 50},
  footer_loader_view: {marginVertical: 20, marginHorizontal: 20},
});

//make this component available to the app
export default Details;
