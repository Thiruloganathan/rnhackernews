//import liraries
import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import MainNavigator from './src/navigation';

// create a component
const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <MainNavigator />
    </SafeAreaView>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

//make this component available to the app
export default App;
